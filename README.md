# Longitudinal Tomography and Beam Parameters Extraction in the LHC using ML

## Contents

### Training data generation:

1. simulations/
2. generate_encoder_data.py
3. generate_decoder_data.py

### Inspect generate training files:
1. inspect_input_data.ipynb

### Model architecture and utilities:
1. models.py
2. utils.py

### Model training:
1. submit_train_trials.py
2. submit_configs/
3. train_decoder.py
4. train_encoder_multiout.py

### Hyperparam optimization:
1. submit_hparam_trial.py
1. train_decoder_hyperparam.py
2. train_encoder_multiout_hyperparam.py


## Trained model evaluation:

1. evaluate_decoder.ipynb: 
2. evaluate_encoder_multiout.ipynb
3. evaluate_end_to_end.ipynb:
4. evaluate_real_data.ipynb:

